package nl.utwente.di.bookQuote;


import java.util.HashMap;

public class Quoter {

    public double getBookPrice(String degree){
        int x = Integer.parseInt(degree);
        return (x * (9.0/5.0)) + 32;
    }
}
